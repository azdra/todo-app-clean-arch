start:
	@docker-compose -f docker-compose.yml -f docker-compose-dev.yml up

build:
	@docker-compose build

enter:
	@docker-compose run --rm node sh

install:
	@docker-compose run --rm node npm install
	@docker compose run --rm react npm install