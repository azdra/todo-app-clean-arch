import { Request } from 'express';
import { readAllTaskUserCases } from '../domain/userCases/task/ReadAllTaskUserCases';
import { IDatabaseAdapters } from '../domain/interfaceAdapters/db/IDatabaseAdapters';
import TaskRepositoryImpl from '../domain/repositories/TaskRepositoryImpl';
import { createTaskUserCases } from '../domain/userCases/task/CreateTaskUserCases';
import Task from '../domain/entities/Task';
import { updateTaskUserCases } from '../domain/userCases/task/UpdateTaskUserCases';
import { deleteTaskUserCases } from '../domain/userCases/task/DeleteTaskUserCases';
import { readTaskUserCases } from '../domain/userCases/task/ReadTaskUserCases';
import { completeTaskUserCases } from '../domain/userCases/task/CompleteTaskUserCases';

export default Object.freeze({
  /**
   * create task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  createTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    const task = new Task();
    task.title = request.body.title;
    task.description = request.body.description;
    task.dueDate = new Date();
    return createTaskUserCases.execute(repository, task);
  },
  /**
   * get one task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  getOneTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    return readTaskUserCases.execute(repository, parseInt(request.params.id));
  },
  /**
   * get all task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  readAllTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    return readAllTaskUserCases.execute(repository);
  },
  /**
   * update task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  updateTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    const task = new Task();
    task.title = request.body.title;
    task.description = request.body.description;
    task.completed = request.body.completed;
    return updateTaskUserCases.execute(repository, parseInt(request.params.id), task);
  },
  /**
   * delete task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  deleteTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    return deleteTaskUserCases.execute(repository, parseInt(request.params.id));
  },
  /**
   * complete task
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Task>}
   */
  completeTask: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new TaskRepositoryImpl(databaseAdapter);
    return completeTaskUserCases.execute(repository, parseInt(request.params.id), request.body.completed);
  }
});



