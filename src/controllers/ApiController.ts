import { JSONAdapters } from '../infra/db/JSONAdapters';
import { SQLiteAdapters } from '../infra/db/SQLiteAdapters';
import { Request, Response } from 'express';


export default (controller) => (req: Request, res: Response) => {
  return controller(req, new SQLiteAdapters()).then((data) => res.json(data))
}