import { Request } from 'express';
import { readAllTaskUserCases } from '../domain/userCases/task/ReadAllTaskUserCases';
import { IDatabaseAdapters } from '../domain/interfaceAdapters/db/IDatabaseAdapters';
import TaskRepositoryImpl from '../domain/repositories/TaskRepositoryImpl';
import { createTaskUserCases } from '../domain/userCases/task/CreateTaskUserCases';
import Task from '../domain/entities/Task';
import { updateTaskUserCases } from '../domain/userCases/task/UpdateTaskUserCases';
import { deleteTaskUserCases } from '../domain/userCases/task/DeleteTaskUserCases';
import { readTaskUserCases } from '../domain/userCases/task/ReadTaskUserCases';
import { completeTaskUserCases } from '../domain/userCases/task/CompleteTaskUserCases';
import InfoRepositoryImpl from '../domain/repositories/InfoRepositoryImpl';
import { getInfoUserCase } from '../domain/userCases/info/GetInfoUserCase';

export default Object.freeze({
  /**
   * get info
   * @param {Request} request
   * @param {IDatabaseAdapters} databaseAdapter
   * @returns {Promise<Info>}
   */
  get: (request: Request, databaseAdapter: IDatabaseAdapters) => {
    const repository = new InfoRepositoryImpl(databaseAdapter);
    return getInfoUserCase.execute(repository);
  }
});



