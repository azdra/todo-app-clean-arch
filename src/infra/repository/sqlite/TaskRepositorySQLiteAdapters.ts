import Task from '../../../domain/entities/Task';
import { ITaskRepository } from '../../../domain/interfaceAdapters/repositories/ITaskRepository';
import { ISqlAdapters } from '../../../domain/interfaceAdapters/db/ISqlAdapters';
import { ISqlRepository } from '../../../domain/interfaceAdapters/repositories/ISqlRepository';

/**
 * @class TaskRepositorySQLiteAdapters
 */
export default class TaskRepositorySQLiteAdapters implements ITaskRepository, ISqlRepository {
  private readonly databaseAdapter: ISqlAdapters;

  /**
   * @constructor
   * @param {ISqlAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: ISqlAdapters) {
    this.databaseAdapter = databaseAdapter;
    this.databaseAdapter.run<void>('CREATE TABLE IF NOT EXISTS task (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, dueDate TEXT, completed BOOLEAN)');
  }

  /**
   * @method createTask
   * @param {Task} task
   * @returns {Promise<Task>}
   */
  public async createTask(task: Task): Promise<Task> {
    return this.databaseAdapter.run<Task>('INSERT INTO task (title, description, dueDate, completed) VALUES (?, ?, ?, ?)', [task.title, task.description, task.dueDate, task.completed]);
  }

  /**
   * @method getTasks
   * @returns {Promise<Task[]>}
   */
  public getTasks(): Promise<Task[]> {
    return this.databaseAdapter.all<Task[]>('SELECT * FROM task');
  }

  /**
   * @method getTaskById
   * @param {number} id
   * @returns {Promise<Task | undefined>}
   */
  public getTaskById(id: number): Promise<Task | undefined> {
    return this.databaseAdapter.get<Task>('SELECT * FROM task WHERE id = ?', [id]);
  }

  /**
   * @method updateTask
   * @param {number} id
   * @param {Task} task
   * @returns {Promise<Task>}
   */
  public updateTask(id: number, task: Task): Promise<Task> {
    return this.databaseAdapter.run<Task>('UPDATE task SET title = ?, description = ? WHERE id = ?', [task.title, task.description, id]);
  }

  /**
   * @method deleteTask
   * @param {number} id
   * @returns {Promise<void>}
   */
  public deleteTask(id: number): Promise<void> {
    return this.databaseAdapter.run<void>('DELETE FROM task WHERE id = ?', [id]);
  }

  /**
   * @method completeTask
   * @param {number} id
   * @param {boolean} status
   * @returns {Promise<Task>}
   */
  public completeTask(id: number, status: boolean): Promise<Task> {
    return this.databaseAdapter.run<Task>('UPDATE task SET completed = ? WHERE id = ?', [status, id]);
  }
}
