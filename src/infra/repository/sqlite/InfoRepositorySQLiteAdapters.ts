import { IInfoRepository } from '../../../domain/interfaceAdapters/repositories/IInfoRepository';
import { ISqlRepository } from '../../../domain/interfaceAdapters/repositories/ISqlRepository';
import { ISqlAdapters } from '../../../domain/interfaceAdapters/db/ISqlAdapters';
import Info from '../../../domain/entities/Info';

/**
 * @class InfoRepositorySQLiteAdapters
 */
export default class InfoRepositorySQLiteAdapters implements IInfoRepository, ISqlRepository {
  private readonly databaseAdapter: ISqlAdapters;

  /**
   * @constructor
   * @param {ISqlAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: ISqlAdapters) {
    this.databaseAdapter = databaseAdapter;
    this.databaseAdapter.run<Info>('CREATE TABLE IF NOT EXISTS info (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT)');
    console.log('InfoRepositorySQLiteAdapters constructor');
  }

  /**
   * @method create
   * @returns {Promise<Info>}
   */
  public get(): Promise<Info> {
    return this.databaseAdapter.get<Info>('SELECT * FROM info');
  }
}
