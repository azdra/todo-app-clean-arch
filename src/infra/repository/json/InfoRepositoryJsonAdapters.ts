import { IInfoRepository } from '../../../domain/interfaceAdapters/repositories/IInfoRepository';
import { IJsonRepository } from '../../../domain/interfaceAdapters/repositories/IJsonRepository';
import { JSONAdapters } from '../../db/JSONAdapters';
import * as util from 'util';
import fs from 'fs';
import Info from '../../../domain/entities/Info';

/**
 * @class InfoRepositoryJsonAdapters
 */
export default class InfoRepositoryJsonAdapters implements IInfoRepository, IJsonRepository {
  private readonly databaseAdapter: JSONAdapters;
  private filePath: string;
  private file: string;


  /**
   * @constructor
   * @param {JSONAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: JSONAdapters) {
    this.databaseAdapter = databaseAdapter;
    this.initialize('%s/database/info.json');
  }

  /**
   * @method initialize
   * @param {string} databaseFilePath
   * @returns {void}
   */
  private initialize(databaseFilePath: string): void {
    this.filePath = this.file = util.format(databaseFilePath, process.cwd());
  }

  /**
   * @method save
   * @param {any} data
   * @returns {void}
   */
  private save(data): void {
    fs.writeFileSync(this.file, JSON.stringify(data));
  }

  /**
   * @method get
   * @returns {Promise<Info>}
   */
  public get(): Promise<Info> {
    return new Promise((resolve, reject) => {
      const data = fs.readFileSync(this.file, 'utf8');
      resolve(JSON.parse(data));
    });
  }
}
