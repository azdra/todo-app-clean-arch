import Task from '../../../domain/entities/Task';
import { ITaskRepository } from '../../../domain/interfaceAdapters/repositories/ITaskRepository';
import { IJsonRepository } from '../../../domain/interfaceAdapters/repositories/IJsonRepository';
import fs from 'fs';
import { JSONAdapters } from '../../db/JSONAdapters';
import * as util from 'util';

/**
 * @class TaskRepositoryJsonAdapters
 */
export default class TaskRepositoryJsonAdapters implements ITaskRepository, IJsonRepository {
  private readonly databaseAdapter: JSONAdapters;
  private filePath: string;
  private file: string;


  /**
   * @constructor
   * @param {JSONAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: JSONAdapters) {
    this.databaseAdapter = databaseAdapter;
    this.initialize('%s/database/tasks.json');
  }

  /**
   * @method initialize
   * @param {string} databaseFilePath
   * @returns {void}
   */
  private initialize(databaseFilePath: string): void {
    this.filePath = this.file =  util.format(databaseFilePath, process.cwd());
  }

  /**
   * @method save
   * @param {any} data
   * @returns {void}
   */
  private save(data): void {
    fs.writeFileSync(this.file, JSON.stringify(data));
  }

  /**
   * @method createTask
   * @param {Task} task
   * @returns {Task}
   */
  public async createTask(task: any): Promise<Task> {
    const tasks = await this.getTasks();
    task.id = new Date().getTime();
    tasks.push(task);
    this.save(tasks);
    return task;
  }

  /**
   * @method getTasks
   * @returns {Task[]}
   */
  public getTasks(): Promise<Task[]> {
    const data = fs.readFileSync(this.filePath, 'utf8');
    return JSON.parse(data);
  }

/**
   * @method getTaskById
   * @param {number} id
   * @returns {Task | undefined}
   */
  public async getTaskById(id: number): Promise<Task | undefined> {
    const tasks = await this.getTasks();
    return tasks.find(task => task.id === id);
  }

  /**
   * @method updateTask
   * @param {number} id
   * @param {Task} task
   * @returns {Task}
   */
  public async updateTask(id: number, task: Task): Promise<Task> {
    const tasks = await this.getTasks();
    const index = tasks.findIndex(task => task.id === id);
    const oldTask = tasks[index];
    task.id = oldTask.id;
    task.dueDate = oldTask.dueDate;
    tasks[index] = task;
    this.save(tasks);
    return task;
  }

  /**
   * @method deleteTask
   * @param {number} id
   * @returns {void}
   */
  public async deleteTask(id: number): Promise<void> {
    const tasks = await this.getTasks();
    const index = tasks.findIndex(task => task.id === id);
    tasks.splice(index, 1);
    this.save(tasks);
  }

  /**
   * @method completeTask
   * @param {number} id
   * @param {boolean} status
   * @returns {Task}
   */
  public async completeTask(id: number, status: boolean): Promise<Task> {
    const tasks = await this.getTasks();
    const index = tasks.findIndex(task => task.id === id);
    const task = tasks[index];
    task.completed = status;
    tasks[index] = task;
    this.save(tasks);
    return task;
  }
}
