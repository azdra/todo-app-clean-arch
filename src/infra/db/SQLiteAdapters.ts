import sqlite3, { Database } from 'sqlite3';
import { ISqlAdapters } from '../../domain/interfaceAdapters/db/ISqlAdapters';
import * as util from 'util';

/**
 * @class SQLiteAdapters
 */
export class SQLiteAdapters implements ISqlAdapters {
  private connection: Database;

  /**
   * @constructor
   */
  public constructor() {
    this.initialize('%s/database/database.db');
  }

  /**
   * @method initialize
   * @param {string} databaseFilePath
   * @returns {void}
   */
  private initialize(databaseFilePath: string) {
    this.connection = new sqlite3.Database(util.format(databaseFilePath, process.cwd()));
  }

  /**
   * @method run
   * @param {string} sql
   * @param {any[]} parameters
   * @template T
   * @returns {Promise<T>}
   */
  public run = <T>(sql: string, parameters: any[] = []): Promise<T> => {
    return this.query<T>('run', sql, parameters);
  };

  /**
   * @method get
   * @param {string} sql
   * @param {any[]} parameters
   * @template T
   * @returns {Promise<T>}
   */
  public get = <T>(sql: string, parameters: any[] = []): Promise<T> => {
    return this.query<T>('get', sql, parameters);
  };

  /**
   * @method all
   * @param {string} sql
   * @param {any[]} parameters
   * @template T
   * @returns {Promise<T[]>}
   */
  public all = <T>(sql: string, parameters: any[] = []): Promise<T[]> => {
    return this.query<T>('all', sql, parameters);
  };

  /**
   * @method query
   * @param {string} method
   * @param {string} query
   * @param {any[]} parameters
   * @template T
   * @returns {Promise<T>}
   */
  public query<T>(method: string, query: string, parameters: any[] = []): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      return this.connection[method](query, parameters, async (err, rows: T) => {
        if (err) {
          reject(err);
        } else {
          if (method === 'run') {
            const regex = /^(INSERT\s+INTO|UPDATE)\s+([a-zA-Z_][a-zA-Z0-9_]*)/i;
            const match = query.match(regex);

            if (match) {
              const tableName = match[2].toLowerCase();

              const { id } = await this.get('SELECT last_insert_rowid() as id');

              resolve(this.get<T>(`SELECT *
                                   FROM ${tableName}
                                   WHERE id = ?`, [id]));
            }
          }
          resolve(rows);
        }
      });
    });
  }
}
