import express from 'express';
import taskRoute from './task.route';
import infoRoute from './info.route';

const router = express.Router();

router.use(taskRoute);
router.use(infoRoute);

export default router;