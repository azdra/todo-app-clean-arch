import express from 'express';

import ApiController from '../controllers/ApiController';
import infoController from '../controllers/info.controller';

const router = express.Router();

router.route('/get-info').get(ApiController(infoController.get));

export default router;