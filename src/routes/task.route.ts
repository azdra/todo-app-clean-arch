import express from 'express';

import taskController from '../controllers/task.controller';
import ApiController from '../controllers/ApiController';

const router = express.Router();

router.route('/create-task').post(ApiController(taskController.createTask));
router.route('/get-one-task/:id').post(ApiController(taskController.getOneTask));
router.route('/update-task/:id').patch(ApiController(taskController.updateTask));
router.route('/delete-task/:id').delete(ApiController(taskController.deleteTask));
router.route('/read-all-task').get(ApiController(taskController.readAllTask));
router.route('/complete-task/:id').patch(ApiController(taskController.completeTask));

export default router;