import express from 'express';
import cors from 'cors';
import routes from './routes';

const port = process.env.BACK_PORT
const app = express();
app.use(express.json()); //Used to parse JSON bodies
app.use(cors())

app.use("/", routes);

app.listen(port, () => {
   console.log(`Example app listening on port ${port}`)
})

export default app;
