import { IRepositoryImpl } from '../interfaceAdapters/repositories/IRepositoryImpl';
import { IDatabaseAdapters } from '../interfaceAdapters/db/IDatabaseAdapters';
import { IInfoRepository } from '../interfaceAdapters/repositories/IInfoRepository';
import { SQLiteAdapters } from '../../infra/db/SQLiteAdapters';
import { JSONAdapters } from '../../infra/db/JSONAdapters';
import Info from '../entities/Info';
import InfoRepositorySQLiteAdapters from '../../infra/repository/sqlite/InfoRepositorySQLiteAdapters';
import InfoRepositoryJsonAdapters from '../../infra/repository/json/InfoRepositoryJsonAdapters';

/**
 * @class InfoRepositoryImpl
 */
export default class InfoRepositoryImpl implements IInfoRepository, IRepositoryImpl {
  private readonly databaseAdapter: IDatabaseAdapters;

  /**
   * @constructor
   * @param {IDatabaseAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: IDatabaseAdapters) {
    this.databaseAdapter = databaseAdapter;
  }

  /**
   * @method getRepository
   * @returns {IInfoRepository}
   */
  public getRepository(): IInfoRepository {
    if (this.databaseAdapter instanceof SQLiteAdapters) {
      return new InfoRepositorySQLiteAdapters(this.databaseAdapter);
    } else if (this.databaseAdapter instanceof JSONAdapters) {
      return new InfoRepositoryJsonAdapters(this.databaseAdapter);
    }
  }

  /**
   * @method get
   * @returns {Promise<Info>}
   */
  public get(): Promise<Info> {
    return this.getRepository().get();
  }
}