import { ITaskRepository } from '../interfaceAdapters/repositories/ITaskRepository';
import { IDatabaseAdapters } from '../interfaceAdapters/db/IDatabaseAdapters';
import Task from '../entities/Task';
import TaskRepositorySQLiteAdapters from '../../infra/repository/sqlite/TaskRepositorySQLiteAdapters';
import TaskRepositoryJsonAdapters from '../../infra/repository/json/TaskRepositoryJsonAdapters';
import { IRepositoryImpl } from '../interfaceAdapters/repositories/IRepositoryImpl';
import { SQLiteAdapters } from '../../infra/db/SQLiteAdapters';
import { JSONAdapters } from '../../infra/db/JSONAdapters';

/**
 * @class TaskRepositoryImpl
 */
export default class TaskRepositoryImpl implements ITaskRepository, IRepositoryImpl {
  private readonly databaseAdapter: IDatabaseAdapters;

  /**
   * @constructor
   * @param {IDatabaseAdapters} databaseAdapter
   */
  public constructor(databaseAdapter: IDatabaseAdapters) {
    this.databaseAdapter = databaseAdapter;
  }

  /**
   * @method getRepository
   * @returns {ITaskRepository}
   */
  public getRepository(): ITaskRepository {
    if (this.databaseAdapter instanceof SQLiteAdapters) {
      return new TaskRepositorySQLiteAdapters(this.databaseAdapter);
    } else if (this.databaseAdapter instanceof JSONAdapters) {
      return new TaskRepositoryJsonAdapters(this.databaseAdapter);
    }
  }

  /**
   * @method createTask
   * @param {Task} task
   * @returns {Task}
   */
  public createTask(task: Task): any {
    return this.getRepository().createTask(task);
  }

  /**
   * @method getTasks
   * @returns {Task[]}
   */
  public async getTasks(): Promise<Task[]> {
    return this.getRepository().getTasks();
  }

  /**
   * @method getTaskById
   * @param {number} id
   * @returns {Task | undefined}
   */
  public async getTaskById(id: number): Promise<Task | undefined> {
    return this.getRepository().getTaskById(id);
  }

  /**
   * @method updateTask
   * @param {number} id
   * @param {Task} task
   * @returns {Task}
   */
  public async updateTask(id: number, task: Task): Promise<Task> {
    return this.getRepository().updateTask(id, task);
  }

  /**
   * @method deleteTask
   * @param {number} id
   * @returns {void}
   */
  public async deleteTask(id: number): Promise<void> {
    return this.getRepository().deleteTask(id);
  }

  /**
   * @method completeTask
   * @param {number} id
   * @param {boolean} status
   * @returns {Task}
   */
  public async completeTask(id: number, status: boolean): Promise<Task> {
    return this.getRepository().completeTask(id, status);
  }
}