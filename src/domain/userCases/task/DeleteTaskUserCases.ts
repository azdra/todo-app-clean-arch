import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const deleteTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @param {number} id
   * @returns {Promise<void>}
   */
  async execute(repository: ITaskRepository, id: number): Promise<void> {
    return repository.deleteTask(parseInt(id.toString()));
  }
}