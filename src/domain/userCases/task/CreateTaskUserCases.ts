import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const createTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @param {Task} task
   * @returns {Promise<Task>}
   */
  async execute(repository: ITaskRepository, task: Task): Promise<Task> {
    return repository.createTask(task);
  }
}