import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const readTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @param {number} id
   * @returns {Promise<Task | undefined>}
   */
  async execute(repository: ITaskRepository, id: number): Promise<Task | undefined> {
    return repository.getTaskById(parseInt(String(id)));
  }
}