import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const completeTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @param {number} id
   * @param {boolean} status
   * @returns {Promise<Task>}
   */
  async execute(repository: ITaskRepository, id: number, status: boolean): Promise<Task> {
    return repository.completeTask(parseInt(String(id)), status);
  }
}