import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const readAllTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @returns {Promise<Task[]>}
   */
  async execute(repository: ITaskRepository): Promise<Task[]> {
    return repository.getTasks();
  }
}