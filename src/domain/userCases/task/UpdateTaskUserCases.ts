import { ITaskRepository } from '../../interfaceAdapters/repositories/ITaskRepository';
import Task from '../../entities/Task';

export const updateTaskUserCases = {
  /**
   * @method execute
   * @param {ITaskRepository} repository
   * @param {number} id
   * @param {Task} task
   * @returns {Promise<Task>}
   */
  async execute(repository: ITaskRepository, id: number, task: Task): Promise<Task> {
    return repository.updateTask(parseInt(String(id)), task);
  }
}