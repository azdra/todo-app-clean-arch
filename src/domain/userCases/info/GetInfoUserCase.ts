import { IInfoRepository } from '../../interfaceAdapters/repositories/IInfoRepository';
import Info from '../../entities/Info';


export const getInfoUserCase = {
  /**
   * @method execute
   * @param {IInfoRepository} repository
   * @returns {Promise<Info>}
   */
  async execute(repository: IInfoRepository): Promise<Info> {
    return repository.get();
  }
}