export default class Task {
  id: number;
  title: string;
  description: string;
  dueDate: Date;
  completed: boolean = false;
}