import Task from '../../entities/Task';

export interface ITaskRepository {
  createTask(task: Task): Promise<Task>;
  getTasks(): Promise<Task[]>;
  getTaskById(id: number): Promise<Task | undefined>;
  updateTask(id: number, task: Task): Promise<Task>;
  deleteTask(id: number): Promise<void>;
  completeTask(id: number, status: boolean): Promise<Task>;
}