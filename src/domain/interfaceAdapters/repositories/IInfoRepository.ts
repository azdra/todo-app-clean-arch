import Info from '../../entities/Info';

export interface IInfoRepository {
  get(): Promise<Info>;
}