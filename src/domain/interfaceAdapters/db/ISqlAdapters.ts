import { IDatabaseAdapters } from './IDatabaseAdapters';

export interface ISqlAdapters extends IDatabaseAdapters {
  query<T>(method: string, value: string, parameters: any[] = []): Promise<T>;
  run<T>(value: string, parameters: any[] = []): Promise<T>;
  get<T>(value: string, parameters: any[] = []): Promise<T>;
  all<T>(value: string, parameters: any[] = []): Promise<T>;
  initialize(databaseFilePath: string): void;
}
